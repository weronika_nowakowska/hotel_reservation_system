import datetime
from logging import getLogger

from django.shortcuts import get_object_or_404, render
from django.urls import reverse_lazy
from django.utils.dateparse import parse_date
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from viewer.forms import ReservationForm
from viewer.models import Room, Reservation, RoomImage, HotelImage

LOGGER = getLogger()


class IndexView(TemplateView):
    template_name = 'index.html'
    images = HotelImage.objects.all()


class RoomsView(ListView):
    template_name = 'rooms.html'
    model = Room

    def get_queryset(self):
        if "date_from" in self.request.GET and "date_to" in self.request.GET:
            start = self.request.GET['date_from']
            end = self.request.GET['date_to']
            reservations = Reservation.find_reservations(start, end)

            room_set = set()
            for reservation in reservations:
                room_set.add(reservation.room_id.id)

            return Room.objects.exclude(id__in=room_set)

        return super(RoomsView, self).get_queryset()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(RoomsView, self).get_context_data(object_list=object_list, **kwargs)
        context['date_from'] = ""
        context['date_to'] = ""

        if 'date_from' in self.request.GET:
            context['date_from'] = self.request.GET['date_from']

        if 'date_to' in self.request.GET:
            context['date_to'] = self.request.GET['date_to']
        return context


def room_detail_view(request, id):
    room = get_object_or_404(Room, id=id)
    photos = RoomImage.objects.filter(room=room)
    return render(request, 'room_detail.html',
                  {'room': room,
                   'photos': photos
                   })


class ReservationsView(LoginRequiredMixin, ListView):
    template_name = 'reservations.html'
    model = Reservation

    def get_queryset(self):
        new_context = Reservation.objects.filter(
            user_id=self.request.user.id
        )
        return new_context


class ReservationDelete():
    pass


class ReservationUpdateView(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    template_name = "form.html"
    model = Reservation
    form_class = ReservationForm
    success_url = reverse_lazy("viewer:reservations")
    permission_required = 'viewer.change_reservation'

    def form_invalid(self, form):
        LOGGER.warning('User provided an invalid data while updating a reservation!')
        return super().form_invalid(form)


class ReservationDetailsView(DetailView):
    template_name = 'reservation_detail.html'
    model = Reservation


class ReservationCreateView(LoginRequiredMixin, CreateView):
    template_name = "form.html"
    form_class = ReservationForm
    model = Room
    pk_url_kwarg = "room_id"

    def get_context_data(self, **kwargs):
        if 'room' not in kwargs:
            kwargs['room'] = self.get_object()
            kwargs['user'] = self.request.user

        context = super().get_context_data(**kwargs)

        if "date_from" in self.request.GET:
            context['form'].fields['start_date'].widget.attrs['value'] = self.request.GET["date_from"]

        if "date_to" in self.request.GET:
            context['form'].fields['end_date'].widget.attrs['value'] = self.request.GET["date_to"]

        result = []
        context['reservations_dates'] = result

        list_of_reservations = Reservation.objects.all()

        for reservation in list_of_reservations:
            date_generated = [reservation.start_date + datetime.timedelta(days=x) for x in range(0, (reservation.end_date - reservation.start_date).days)]
            for date in date_generated:
                result.append(date.strftime('%Y-%m-%d'))

        return context

    def get_success_url(self):
        return reverse_lazy('viewer:reservation_detail', kwargs={'pk': self.object.id})

    def form_invalid(self, form):
        LOGGER.warning('User provided an invalid data!')
        return super().form_invalid(form)

    def get_form_kwargs(self):
        kwargs = super(ReservationCreateView, self).get_form_kwargs()

        if self.request.method in ('POST', 'PUT'):
            kwargs['data'] = {
                'user_id': self.request.user.id,
                'room_id': self.kwargs['room_id'],
            }

            for key, value in self.request.POST.items():
                kwargs['data'][key] = value

            # cena
            room = Room.objects.get(id=kwargs['data']['room_id'])

            # https://stackoverflow.com/questions/38528073/parsing-dates-with-django-utils-parse-date
            kwargs['data']['start_date'] = datetime.datetime.strptime(kwargs['data']['start_date'], "%Y-%m-%d").date()
            kwargs['data']['end_date'] = datetime.datetime.strptime(kwargs['data']['end_date'], "%Y-%m-%d").date()

            number_of_days = (kwargs['data']['end_date'] - kwargs['data']['start_date']).days
            kwargs['data']['price'] = room.current_price * number_of_days

        return kwargs
