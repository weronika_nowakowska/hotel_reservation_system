from django.contrib import admin
from django.contrib.admin import ModelAdmin, site
# Register your models here.

from viewer.models import Room, RoomType, Reservation, User, RoomImage, HotelImage


admin.site.register(RoomType)
admin.site.register(HotelImage)


@admin.register(Reservation)
class ReservationAdmin(ModelAdmin):

    list_filter = ['user_id', 'room_id']
    list_display = ['user_id', 'room_id', 'start_date', 'end_date', 'ts_created']


class RoomImageAdmin(admin.StackedInline):
    model = RoomImage


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    inlines = [RoomImageAdmin]
    list_display = ('name', 'room_type_id', 'current_price', 'place_in_room', 'current_status')
    list_editable = ['current_price', 'current_status']
    list_filter = ['room_type_id', 'current_price', 'place_in_room', 'current_status']

    class Meta:
        model = Room


@admin.register(RoomImage)
class RoomImageAdmin(admin.ModelAdmin):
    list_display = ['room', 'images']
    list_filter = ['room']






