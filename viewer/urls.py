from django.urls import path
from viewer.views import(
    IndexView,
    RoomsView,
    ReservationCreateView,
    ReservationsView,
    ReservationDetailsView,
    ReservationUpdateView, room_detail_view

)
app_name = 'viewer'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('room/<int:id>/', room_detail_view, name='room_details'),
    path('rooms/', RoomsView.as_view(), name='rooms'),
    path('reservations/', ReservationsView.as_view(), name='reservations'),
    path('reservation/create/<int:room_id>/', ReservationCreateView.as_view(), name='reservation_create'),
    path('reservation/update/<int:pk>/', ReservationUpdateView.as_view(), name='reservation_update'),
    path('reservation/<int:pk>/', ReservationDetailsView.as_view(), name='reservation_detail'),
]
