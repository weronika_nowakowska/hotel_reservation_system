# Generated by Django 3.2.2 on 2021-05-25 16:44

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='HotelImage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('images', models.FileField(upload_to='images/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['bmp', 'jpg', 'jpeg', 'jpe', 'gif', 'tif', 'tiff', 'png'])])),
            ],
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('place_in_room', models.IntegerField()),
                ('description', models.TextField()),
                ('current_price', models.IntegerField(default=False)),
                ('current_status', models.CharField(choices=[('1', 'wolny'), ('2', 'zajęty'), ('3', 'w_remoncie')], max_length=128)),
                ('ts_created', models.DateTimeField(auto_now_add=True)),
                ('image', models.FileField(blank=True, upload_to='')),
            ],
        ),
        migrations.CreateModel(
            name='RoomType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='RoomRecovery',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('room_id', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='viewer.room')),
            ],
        ),
        migrations.CreateModel(
            name='RoomImage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('images', models.FileField(upload_to='images/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['bmp', 'jpg', 'jpeg', 'jpe', 'gif', 'tif', 'tiff', 'png'])])),
                ('room', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='viewer.room')),
            ],
        ),
        migrations.AddField(
            model_name='room',
            name='room_type_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='viewer.roomtype'),
        ),
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
                ('ts_created', models.DateTimeField(auto_now_add=True)),
                ('room_id', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='viewer.room')),
                ('user_id', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
