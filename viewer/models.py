from django.db.models import (
    Model,
    CharField,
    ForeignKey,
    DO_NOTHING,
    IntegerField,
    TextField,
    DateTimeField,
    DateField,
    EmailField, FileField, CASCADE
)
from django.core.validators import FileExtensionValidator
from accounts.models import User


class RoomType(Model):
    name = CharField(max_length=128)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class Room(Model):
    CHOICES = [
        ('1', 'wolny'),
        ('2', 'zajęty'),
        ('3', 'w_remoncie'),
    ]
    name = CharField(max_length=128)
    room_type_id = ForeignKey(RoomType, on_delete=DO_NOTHING)
    place_in_room = IntegerField()
    description = TextField()
    current_price = IntegerField(default=False)
    current_status = CharField(max_length=128, choices=CHOICES)
    ts_created = DateTimeField(auto_now_add=True)
    image = FileField(blank=True)

    def __str__(self):
        return f'{self.name}, {self.current_price}, {self.current_status}'

    def __repr__(self):
        return f'{self.name}, {self.current_price}, {self.current_status}'


class RoomImage(Model):
    images = FileField(upload_to='images/', validators=[FileExtensionValidator(
        allowed_extensions=['bmp', 'jpg', 'jpeg', 'jpe', 'gif', 'tif', 'tiff', 'png']
    )])
    room = ForeignKey(Room, default=None, on_delete=CASCADE)

    def __str__(self):
        return self.room.name


class HotelImage(Model):
    images = FileField(upload_to='images/', validators=[FileExtensionValidator(
        allowed_extensions=['bmp', 'jpg', 'jpeg', 'jpe', 'gif', 'tif', 'tiff', 'png']
    )])


class Reservation(Model):
    user_id = ForeignKey(User, on_delete=DO_NOTHING)
    room_id = ForeignKey(Room, on_delete=DO_NOTHING)
    start_date = DateField()
    end_date = DateField()
    ts_created = DateTimeField(auto_now_add=True)
    price = IntegerField(blank=False)

    @staticmethod
    def find_reservations(begin_range, end_range):
        reservations = Reservation.objects.filter(start_date__lte=begin_range, end_date__gt=begin_range)
        reservations = reservations | Reservation.objects.filter(start_date__lt=end_range, end_date__gte=end_range)
        reservations = reservations | Reservation.objects.filter(start_date__gte=begin_range, start_date__lt=end_range)
        reservations = reservations | Reservation.objects.filter(end_date__gt=begin_range, end_date__lte=end_range)

        return reservations

    def __str__(self):
        return f'id użytkownika: {self.user_id}, id rezerwacji: {self.id}'

    def __repr__(self):
        return f'id użytkownika: {self.user_id}, id rezerwacji: {self.id}'




class RoomRecovery(Model):
    start_date = DateTimeField()
    end_date = DateTimeField()
    room_id = ForeignKey(Room, on_delete=DO_NOTHING)
