from datetime import date

from django.core.exceptions import ValidationError
from django.forms import ModelForm

from viewer.models import Reservation


class ReservationForm(ModelForm):
    class Meta:
        model = Reservation
        fields = ['start_date', 'end_date', 'user_id', 'room_id', 'price']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            visible.field.widget.attrs['placeholder'] = visible.label
            visible.field.widget.attrs['autocomplete'] = "off"

    def clean(self):
        result = super().clean()
        start = result['start_date']
        end = result['end_date']

        if start >= end:
            raise ValidationError("Start date must be l than end date")
        if start < date.today() or end < date.today():
            raise ValidationError("Past dates not allowed!!")

        reservations = Reservation.find_reservations(start, end)

        room_set = set()
        for reservation in reservations:
            room_set.add(reservation.room_id.id)

        if result['room_id'].id in room_set:
            raise ValidationError(" This room has already been booked for this date.")

        return result


