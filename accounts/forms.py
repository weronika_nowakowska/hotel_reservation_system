from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, UsernameField
from django.utils.translation import gettext, gettext_lazy as _

from accounts.models import User


class SignInForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(SignInForm, self).__init__(*args, **kwargs)

    username = UsernameField(
        label=_("Username"),
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Username',
            }
        ),
    )
    password = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Password',
            }
        ),
    )


class SignUpForm(UserCreationForm):
    username = forms.CharField(
        label=_('Username'),
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Username',
            }
        ),
        max_length=150, required=True
    )

    email = forms.EmailField(
        label=_('Address e-mail'),
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control spaced',
                'placeholder': 'Address e-mail',
            }
        ),
        max_length=254, required=True
    )

    first_name = forms.CharField(
        label=_('Fist name'),
        widget=forms.TextInput(
            attrs={
                'autocomplete': 'given-name',
                'class': 'form-control',
                'placeholder': 'Fist name',
            }
        ),
        max_length=150, required=False
    )
    last_name = forms.CharField(
        label=_('Last name'),
        widget=forms.TextInput(
            attrs={
                'autocomplete': 'family-name',
                'class': 'form-control',
                'placeholder': 'Last name',
            }
        ),
        max_length=150, required=False
    )
    phone_number = forms.CharField(
        label=_('Phone number'),
        widget=forms.TextInput(
            attrs={
                'autocomplete': 'tel',
                'class': 'form-control spaced',
                'placeholder': 'Phone number',
            }
        ),
        max_length=15, required=False
    )

    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={
            'autocomplete': 'new-password',
            'class': 'form-control',
            'placeholder': 'Password',
        }),
        help_text=password_validation.password_validators_help_text_html(),
    )

    password2 = forms.CharField(
        label=_("Password (again)"),
        widget=forms.PasswordInput(attrs={
            'autocomplete': 'new-password',
            'class': 'form-control spaced',
            'placeholder': 'Password (again)',
        }),
        strip=False,
    )

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
            'phone_number',
            'password1',
            'password2',
        )
