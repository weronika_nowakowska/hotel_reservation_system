from django.db.models import Model, CharField, IntegerField, EmailField

from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    phone_number = CharField(_('phone number'), max_length=15, default="", blank=True)

    def __str__(self):
        return f'[{self.id}] {self.username} ({self.first_name}, {self.last_name})'

    def __repr__(self):
        return f'[{self.id}] {self.first_name}, {self.last_name}'
