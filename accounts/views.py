from django.contrib.auth.views import LoginView
from django.contrib.auth import logout, authenticate, login

from django.shortcuts import redirect, render

from accounts.forms import SignInForm, SignUpForm


class SignInView(LoginView):
    template_name = 'sign_in.html'
    form_class = SignInForm

    def form_invalid(self, form):
        for field in ('username', 'password'):
            form[field].field.widget.attrs['class'] += ' is-invalid'
        return super(SignInView, self).form_invalid(form)


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('viewer:index')
        else:
            for field in form.errors:
                form[field].field.widget.attrs['class'] += ' is-invalid'

    else:
        form = SignUpForm()

    return render(request, 'sign_up.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect('viewer:index')

