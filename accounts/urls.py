from django.urls import path

from accounts.views import SignInView, logout_view, signup

app_name = 'accounts'

urlpatterns = [
    path('signin/', SignInView.as_view(), name='signin'),
    path('signup/', signup, name='signup'),
    path('logout/', logout_view, name='logout'),

]