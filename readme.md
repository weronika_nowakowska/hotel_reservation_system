# Hotel reservation system
### Żeby wystartować projekt należy wykonac po kolei następujące komendy:
```
git clone https://gitlab.com/weronika_nowakowska/hotel_reservation_system.git
```
Uruchomienie środowiska wirtualnego

Dla systemu Linux:
```
python3 -m pip install --user virtualenv
python3 -m venv venv
source env/bin/activate
```
Dla systemu Windows:
```
py -m pip install --user virtualenv
py -m venv venv
.\env\Scripts\activate
```
Gdy już mamy aktywne środowisko wirtualne, wykonujemy komendy:
```
pip install -r requirement.txt
python manage.py migrate
```
Natępnie należy stworzyć admina. Służy do tego komenda:
```
python manage.py createsuperuser
```
Po wywołaniu powyższej komendy zakładamy admina.

Aby dodać rekordy z poziomu admina, należy uruchomić serwer.Służy do tego komenda:
```
python manage.py runserver
```
Po wykonaniu powyższej komendy należy kliknąć w adres **127.0.0.1:8000**, 
a następnie przechodząc na stronie, w pasku adresu dopisac **/admin** i nacisnąć enter.

Po zalogowaniu się na panel administratora, należy dodać rekordy do tabel:
- RoomType
- Rooms

W przypadku Rooms, należy dołączyć zdjęcia:
- 1 główne
- 3 do każdego pokoju
### Inne możliwości dodawania rekordów
Aby dodać rekordy z poziomu terminala należy wykonać następujące komendy:
```
python manage.py shell
from viewer.models import Room, RoomType
```
Aby dodać typy pokoi należy wykonac komendę:
```
RoomType.objects.create(name='your_name_of_room_type')
```
lub
```
room_type = RoomType(name='your_name_of_room_type')
room_type = save()
```
Do sprawdzenia listy zapisanych w bazie typów pokoi, służy komenda:
```
RoomType.objects.all()
```
Aby dodać pokoje należy wykonać komendę:
```
 Room.objects.create(name='Your_name_of_room', place_in_room=2, description="Opis pokoju", current_price=123, current_status=1, room_type_id=RoomType.objects.get(id=1))
```
lub
```
room = Room(name='Your_name_of_room', place_in_room=2, description="Opis pokoju", current_price=123, current_status=1, room_type_id=RoomType.objects.get(id=1))
room.save()
```
Do sprawdzenia listy zapisanych w bazie pokoi służy komenda:
```
Room.objects.all()
```
### Funkcjonalności
- Zakładanie konta przez użytkownika,
- logowanie użytkownika,
- widok rezerwacji dla konkretnego użytkownika,
- zakładanie rezerwacji w wybranym przez użytkownika terminie,
- walidacja na polach zakładania rezerwacji,
- wyłączenie z kalendarza dat, w których istnieje już rezerwacja,
- dodawanie pokoju z poziomu panelu administratora,
- dodawanie zdjęć do pokoi z poziomu panelu administratora,
- dodawanie zdjęć do hotelu z poziomu panelu administratora,
- dodtosowanie panelu administratora dla pracownika hotelu.







